from micropyGPS import MicropyGPS
import machine
from machine import UART
from machine import I2C
import utime
from math import sin, cos, sqrt, atan2, radians


class Location:
    """Class for locking the GPS and retrieving the current location."""
    def __init__(self):
        super(Location, self).__init__()
        print("Initializing GPS...")
        self.i2c = None
        self.gps = None
        self.raw = bytearray(1)
        self.prev_data = {}
        self.last_data = {}

    def setup(self):
        self.i2c = machine.I2C(0, mode=I2C.MASTER, pins=('P22', 'P21'))
        GPS_I2CADDR = const(0x10)
        self.i2c.writeto(GPS_I2CADDR, self.raw)
        self.gps = MicropyGPS(location_formatting='dd')
        print("Start reading GPS data...")

    def get_coordinates(self):
        self.raw = self.i2c.readfrom(GPS_I2CADDR, 16)
        for b in self.raw:
            sentence = self.gps.update(chr(b))
            if sentence is not None:
                res = self.check_for_valid_coordinates(self.gps)
                return res
        return False

    def check_for_valid_coordinates(self, gps):
        if gps.satellite_data_updated() and gps.valid:
            self.last_data['timestamp'] = gps.timestamp
            self.last_data['date'] = gps.date
            self.last_data['latitude'] = gps.latitude[0]
            self.last_data['longitude'] = gps.longitude[0]
            return True
        return False

    def print(self):
        if 'date' in self.last_data:
            d = self.last_data['date']
            ts = self.date_to_ts(self.last_data['date'], self.last_data['timestamp'])
            print("@ ", ts)
        if 'latitude' in self.last_data and 'longitude' in self.last_data:
            print("> ", self.last_data['latitude'], " - ", self.last_data['longitude'])

    def date_to_ts(self, date, time):
        d = list(reversed(date))  # fix the date format from (d, m, Y) to (Y m d)
        d[0] += 2000  # 4 digit year (should be safe for the next millenia)
        ts = d + list(time) + [0, 0, 0]  # add date, time, etc
        ts = [int(x) for x in ts]  # make sure it's all ints
        ts = utime.mktime(tuple(ts))  # convert to TS
        return ts

    def get_data(self):
        return [self.last_data['latitude'], self.last_data['longitude'],
        self.date_to_ts(self.last_data['date'], self.last_data['timestamp'])]

    def distance(alat, alng, blat, blng):
        R = 6373.0  # radius of the earth
        a_lat = radians(alat)  # python needs radians
        a_lng = radians(alng)
        b_lat = radians(blat)
        b_lng = radians(blng)

        dlat = b_lat - a_lat
        dlon = b_lng - a_lng

        a = sin(dlat/2)**2 + cos(a_lat) * cos(b_lat) * sin(dlon/2)**2
        c = 2 * atan2(sqrt(a), sqrt(1-a))
        d = R * c
        return d

    def stop(self):
        print("Closing GPS...")
        self.i2c.deinit()
