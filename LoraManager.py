from network import LoRa
import socket
import ubinascii
import struct
import utime
from Config import Config


class LoraManager:
    """Class handling the LoRa connection and messages."""
    def __init__(self):
        super(LoraManager, self).__init__()
        self.lora = None
        # OTAA authentication credentials
        self.app_eui = ubinascii.unhexlify(Config.APP_EUI)
        self.app_key = ubinascii.unhexlify(Config.APP_KEY)
        self.socket = None

    def setup(self):
        self.lora = LoRa(mode=LoRa.LORAWAN, region=LoRa.EU868)
        self.socket = socket.socket(socket.AF_LORA, socket.SOCK_RAW)
        self.socket.setsockopt(socket.SOL_LORA, socket.SO_DR, 5)

    def connect(self):
        self.lora.join(activation=LoRa.OTAA, auth=(self.app_eui, self.app_key), timeout=0)
        utime.sleep(2)
        print("Trying to connect...", end="")
        while not self.lora.has_joined():
            utime.sleep(3)
            print('.', end="")
        print("\nConnected!")

    def is_connected(self):
        if self.lora.has_joined():
            return True
        return False

    def send(self, msg):
        self.socket.setblocking(True)
        print(msg)
        self.socket.send(msg)
        self.socket.setblocking(False)
