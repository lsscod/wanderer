import gc
from machine import Pin
from machine import Timer
import utime
import sys
from Location import Location
from LoraManager import LoraManager
from Button import Button


# enable gc to save some memory
gc.enable()

enable_gps = True
enable_lora = True

button = Button('P10')

LORA_AIRTIME = 60  # the seconds needed waiting to send the next message
MIN_GPS_DISTANCE = 15  # minimun distance in meters
timer = Timer.Chrono()
timer.start()

def test_lora():
    if not loraManager.is_connected():  # make sure we're connected to a gateway
        loraManager.connect()
    res = [56.12091, 10.15903, 1544092949]
    loc_msg = ' '.join(str(e) for e in res)  # array to comma separeted string
    msg = loc_msg + " " + str(1)
    loraManager.send(msg)
    sys.exit()

loc = Location()
loc.setup()
i = 0
print("Trying to get a GPS lock...")
while i < 5 and enable_gps:
    if loc.get_coordinates():
        print(loc.get_data())
        print()
        i += 1
    utime.sleep_ms(50)
print("GPS locked")

loraManager = LoraManager()
loraManager.setup()
# test_lora()

times = 0
loc_msg = ""
status = 0
prev_coord = [0, 0]  # initialize previous coordinates to (0, 0)
while times < 10:  # FIXME: use an endless loop
    while True:
        if button.updated():
            if button.get_click():
                print("Btn clicked: Status Alert!")
                status = 1

        if timer.read() > LORA_AIRTIME:
            if not loraManager.is_connected() and enable_lora:  # make sure we're connected to a lora gateway
                loraManager.connect()
            if loc.get_coordinates() and enable_gps:
                res = loc.get_data()
                curr_dist = Location.distance(res[0], res[1], prev_coord[0], prev_coord[1]) * 1000
                if curr_dist < MIN_GPS_DISTACE and status == 0:  # skip normal status where coords are close
                    print("Dist < MIN_GPS_DISTACE: ", curr_dist)
                    break
                print("Distance over {} meters".format(MIN_GPS_DISTANCE))
                loc_msg = ' '.join(str(e) for e in res)  # array to comma separeted string
                msg = loc_msg + " " + str(status)
                if loraManager.is_connected():
                    loraManager.send(msg)
                timer.reset()
                timer.start()
                status = 0  # reset status to normal
                prev_coord[0] = res[0]
                prev_coord[1] = res[1]
                times += 1
                break

loc.stop()  # cleanup location object (if it ever reaches this state)
