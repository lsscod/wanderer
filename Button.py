from machine import Pin


class Button:
    def __init__(self, pin):
        self.button = Pin(pin, Pin.IN, Pin.PULL_UP)

        self.counter = 0
        self.current_state = 0
        self.debounce_count = 50
        self.debounced = False
        self.clicks = 0

    def is_pressed(self):
        p = self.button.value()
        return not p  # return: 1==pressed, 0==released

    def updated(self):
        reading = self.is_pressed()

        if reading == self.current_state and self.counter > 0:
            self.counter -= 1
        if reading != self.current_state:
            self.counter += 1
        if self.counter >= self.debounce_count:
            self.counter = 0
            self.current_state = reading
            self.debounced = True
        else:
            self.debounced = False

        return self.debounced

    def get_state(self):
        self.clicks += 1
        return self.current_state

    def get_click(self):
        if not self.current_state % 2:
            return True
        return False
